<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\Question;
use Application\Entity\Answer;

/**
 * This class represents iframes.
 * @ORM\Entity(repositoryClass="\Application\Repository\IframeRepository")
 * @ORM\Table(name="iframe")
 */
class Iframe
{
    const STATUS_ACTIVE       = 1;
    const STATUS_INACTIVE     = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name",type="string")
     */
    protected $name;

    /**
     * @ORM\Column(name="email_report",type="string", length=50)
     */
    protected $emailReport;

    /**
     * @ORM\Column(name="email_comment",type="string", length=50)
     */
    protected $emailComment;

    /**
     * @ORM\Column(name="icon_color",type="string", length=10)
     */
    protected $iconColor;

    /**
     * @ORM\Column(name="icon_button_data",type="string")
     */
    protected $iconButtonData;

    /**
     * @ORM\Column(name="iframe_bg_color",type="string", length=10)
     */
    protected $iframeBgColor;

    /**
     * @ORM\Column(name="icon_folder",type="string")
     */
    protected $iconFolder;

    /**
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     * @ORM\Column(name="completed_image",type="string")
     */
    protected $completedImage;

    /**
     * @ORM\Column(name="completed_image_url",type="string")
     */
    protected $completedImageUrl;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Language", inversedBy="iframe")
     * @ORM\JoinColumn(name="default_language", referencedColumnName="id")
     */
    protected $defaultLanguage;

    /**
     * @ORM\OneToMany(targetEntity="\Application\Entity\Question", mappedBy="iframe", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="id", referencedColumnName="iframe_id")
     */
    protected $questions;

    /**
     * @ORM\OneToMany(targetEntity="\Application\Entity\Answer", mappedBy="iframe", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="id", referencedColumnName="iframe_id")
     */
    protected $answers;

    /**
     * @ORM\Column(name="date_created")
     */

    protected $dateCreated;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->answers = new ArrayCollection();
    }

    /**
     * Returns iframe ID.
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets iframe ID.
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns name.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets name.
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns emailReport.
     * @return string
     */
    public function getEmailReport()
    {
        return $this->emailReport;
    }

    /**
     * Sets emailReport.
     * @param string $emailReport
     */
    public function setEmailReport($emailReport)
    {
        $this->emailReport = $emailReport;
    }

    /**
     * Returns emailComment.
     * @return string
     */
    public function getEmailComment()
    {
        return $this->emailComment;
    }

    /**
     * Sets emailComment.
     * @param string $emailComment
     */
    public function setEmailComment($emailComment)
    {
        $this->emailComment = $emailComment;
    }

    /**
     * Returns iconColor.
     * @return string
     */
    public function getIconColor()
    {
        return $this->iconColor;
    }

    /**
     * Sets iconColor.
     * @param string $iconColor
     */
    public function setIconColor($iconColor)
    {
        $this->iconColor = $iconColor;
    }
    /**
     * Returns iconButtonData.
     * @return string
     */
    public function getIconButtonData()
    {
        return json_decode($this->iconButtonData, true);
    }

    /**
     * Sets name.
     * @param string $iconButtonData
     */
    public function setIconButtonData($iconButtonData)
    {
        $this->iconButtonData = $iconButtonData;
    }
    /**
     * Returns iframeBgColor.
     * @return string
     */
    public function getIframeBgColor()
    {
        return $this->iframeBgColor;
    }

    /**
     * Sets iframeBgColor.
     * @param string $iframeBgColor
     */
    public function setIframeBgColor($iframeBgColor)
    {
        $this->iframeBgColor = $iframeBgColor;
    }

    /**
     * Returns iconFolder.
     * @return string
     */
    public function getIconFolder() {
        return $this->iconFolder;
    }
    /**
     * Returns icons Array.
     * @return array
     */
    public function getIcons() {
        $icons = [];
        $path = realpath($_SERVER['DOCUMENT_ROOT'] .'/img/smiles/'.DIRECTORY_SEPARATOR.$this->iconFolder);
        if(is_dir($path)) {
            $icons = glob($path. '/*.svg');
        }
        return $icons;
    }

    /**
     * Sets iconFolder.
     * @param string $iconFolder
     */
    public function setIconFolder($iconFolder)
    {
        $this->iconFolder = $iconFolder;
    }

    /**
     * Returns status.
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns possible statuses as array.
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive'
        ];
    }

    /**
     * Returns iframe status as string.
     * @return string
     */
    public function getStatusAsString()
    {
        $list = self::getStatusList();
        if (isset($list[$this->status]))
            return $list[$this->status];

        return 'Unknown';
    }

    /**
     * Sets status.
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Returns defaultLanguage.
     * @return string
     */
    public function getDefaultLanguage()
    {
        return $this->defaultLanguage;
    }

    /**
     * Returns completedImage.
     * @return string
     */
    public function getCompletedImage()
    {
        return $this->completedImage;
    }

    /**
     * Sets completedImage.
     * @param int $completedImage
     */
    public function setCompletedImage($completedImage)
    {
        $this->completedImage = $completedImage;
    }

    /**
     * Returns completedImageUrl.
     * @return string
     */
    public function getCompletedImageUrl()
    {
        return $this->completedImageUrl;
    }

    /**
     * Sets completedImageUrl.
     * @param int $completedImageUrl
     */
    public function setCompletedImageUrl($completedImageUrl)
    {
        $this->completedImageUrl = $completedImageUrl;
    }

    /**
     * Sets defaultLanguage.
     * @param int $defaultLanguage
     */
    public function setDefaultLanguage($defaultLanguage)
    {
        $this->defaultLanguage = $defaultLanguage;
    }

    /**
     * Returns questions for this iframe.
     * @return array
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Returns questions for this iframe by type.
     * @return array
     */
    public function getQuestionsByType($type = null)
    {
        $result = [];
        foreach($this->questions as $question) {

            if (strtolower($question->getType()) === strtolower($type)){
                $result[$question->getCollection()][] = $question;
            }
        }

        return $result;
    }

    /**
     * Returns questions for this iframe by type, language and collection.
     * @return array
     */
    public function getQuestionContentByData($type = null, $lang_id = null, $collection = 0)
    {
        $result = ["id" => "", "content" => ""];
        foreach($this->questions as $question) {
            if (strtolower($question->getType()) === strtolower($type)
                && $question->getCollection() == $collection ){
                $result = ["id" => $question->getId(), "content" => $question->getQuestionContentByLanguage($lang_id)];
                break;
            }
        }
        return $result;
    }


    /**
     * Adds a new question to this iframe.
     * @param $question
     */
    public function addQuestion($question)
    {
        $this->questions[] = $question;
    }

    /**
     * Returns answers for this iframe.
     * @return array
     */
    public function getAnswers()
    {
        return $this->answers;
    }


    /**
     * Adds a new answer to this iframe.
     * @param $answer
     */
    public function addAnswer($answer)
    {
        $this->answers[] = $answer;
    }

    /**
     * Returns the date of iframe creation.
     * @return string
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Sets the date when this iframe was created.
     * @param string $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

}