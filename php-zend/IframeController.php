<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Iframe;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;
use Application\Entity\Language;
use Zend\Paginator\ScrollingStyle\Sliding;

class IframeController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Iframe manager.
     * @var User\Service\IframeManager
     */
    private $iframeManager;

    /**
     * Constructor.
     */
    public function __construct($entityManager, $iframeManager)
    {
        $this->entityManager = $entityManager;
        $this->iframeManager = $iframeManager;
        $this->layout()->setTemplate('application/layout');
    }

    public function indexAction()
    {
        $this->layout()->setTemplate('application/layout');
        $page = $this->params()->fromQuery('page', 1);

        $query = $this->entityManager->getRepository(Iframe::class)
            ->findAllIframes();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);
//        Paginator::setDefaultScrollingStyle(new Sliding());
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(10);

        return new ViewModel([
            'data' => $paginator
        ]);
    }

    public function createAction()
    {
        $this->layout()->setTemplate('application/layout');
        $imagesDir = $_SERVER['DOCUMENT_ROOT'] .'/img/smiles/';
        $smilesData = $this->getSmileyIcons($imagesDir);
        $languages = $this->getLanguagesArray();
        $defaultButtonsData = [["color" => "#ff565f", "text" => "Not good"], ["color" => "#ffd018", "text" => "Ok"], ["color" => "#64ca64", "text" => "Excellent"]];
        if ($this->getRequest()->isPost()) {
//            $data = $this->params()->fromPost();
            $data = array_merge_recursive(
                $this->getRequest()->getPost()->toArray(),
                $this->getRequest()->getFiles()->toArray()
            );
            if(!$this->validateImage($data['completed_image'])) {
                return $this->redirect()->back();
            }
            $this->iframeManager->addIframe($data);
            return $this->redirect()->toRoute('iframe');
        }

        return new ViewModel([
            'smilesData' => $smilesData ?: [],
            'languages' => $languages ?: [],
            'defaultButtonsData' => $defaultButtonsData
        ]);
    }

    private function validateImage($img) {
        $allowedExtension = array('jpg', 'jpeg', 'png', 'bmp', 'gif');

        $extension = explode('.', $img['name']);
        $extension = strtolower(end($extension));

        return (0 === $img['error'] && in_array($extension, $allowedExtension));

    }
    private function getSmileyIcons($dir){
        $dirs = array_diff(scandir($dir), [".", ".."]);
        $result = [];
        foreach($dirs as $key => $value){
            $path = realpath($dir.DIRECTORY_SEPARATOR.$value);

            if(is_dir($path)) {
                $result[$value] = glob($path. '/*.svg');
            }
        }

        return $result;
    }

    private function getLanguagesArray() {
        $languages =  $this->entityManager->getRepository(Language::class)
            ->findAllLanguages();
        $result = [];
        foreach($languages as $key => $language){
            $result[] = ["id" => $language->getId(), "name" => $language->getName()];
        }
        return $result;

    }

    public function viewAction() {
        $this->layout()->setTemplate('application/layout');
        $id = $this->params()->fromRoute('id', null);

        $item = $this->iframeManager->getIframe($id);
        if(!$item) {
            return $this->redirect()->toRoute('iframe');
        }
        $languages = $this->getLanguagesArray();
        return new ViewModel([
            'item' => $item,
            'languages' =>  $languages
        ]);
    }

    public function editAction() {
        $this->layout()->setTemplate('application/layout');
        $id = $this->params()->fromRoute('id', null);

        $item = $this->iframeManager->getIframe($id);

        if(!$item) {
            return $this->redirect()->toRoute('iframe');
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $this->iframeManager->updateIframe($id, $data);
            return $this->redirect()->toRoute('iframe');
        }

        $imagesDir = $_SERVER['DOCUMENT_ROOT'] .'/img/smiles/';
        $smilesData = $this->getSmileyIcons($imagesDir);
        $languages = $this->getLanguagesArray();


        return new ViewModel([
            'item' => $item,
            'smilesData' => $smilesData ?: [],
            'languages' => $languages ?: []
        ]);
    }

    public function deleteAction() {
        $id = $this->params()->fromRoute('id', null);

        $this->iframeManager->deleteIframe($id);

        return $this->redirect()->toRoute('iframe');
    }
}

