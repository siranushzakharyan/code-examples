<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\Iframe;

/**
 * This is the custom repository class for Iframe entity.
 */
class IframeRepository extends EntityRepository
{
    /**
     * Retrieves all iframes in descending dateCreated order.
     * @return Query
     */
    public function findAllIframes()
    {
        $entityManager = $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
        
        $queryBuilder->select('i')
            ->from(Iframe::class, 'i')
            ->orderBy('i.dateCreated', 'DESC');
        
        return $queryBuilder->getQuery();
    }
}