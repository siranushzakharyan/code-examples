import React from 'react';
import GeoLocation from '../../geolocation/container/geolocation';
import {IHomeState} from "../redux/reducers";
import {getError, getHome} from "../redux/selectors";
import {HomeActionCreators} from "../redux/actions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import parse from "html-react-parser";
import {BASE_API_GATEWAY} from "../../../utils/constants/service";
import "./home.scss";

import Banner from "./components/banner";
import Spirit from "./components/spirit";
import Category from "./components/category";
import PageMetaData from "../../common/pageMetaData/pageMetaData";

interface Props extends IHomeState {
    data: any,
    error: any,
}

class Home extends React.Component<Props & typeof HomeActionCreators> {

    componentDidMount() {
        Object.keys(this.props.menuItem).length ? this.props.homeStart(this.props.menuItem.gridId) : "";
    }
    getData = (key, type = "text") => {
        let result;
        const itemByKey = this.props.data.length ? this.props.data.find(item => item.name === key) : "";
        const regex = /(<([^>]+)>)/ig;
        switch(type) {
            case "text":
                result = itemByKey ? parse(itemByKey.value.replace(regex, '')) : "";
                break;
            case "image":
                result = itemByKey ? (BASE_API_GATEWAY + itemByKey.value) : "";
                break;
            case "menu-href":
                result = itemByKey ? itemByKey.menuHref : "/#";
                break;
            default:
                result = "";
        }
        return result;
    };
    render() {
        const {menuItem} = this.props;
        return (
            <main>
                {menuItem && Object.keys(menuItem).length &&
                <PageMetaData pageKey={menuItem.key}/>
                }

                <Banner getData={this.getData}/>

                <Category keys={null} getData={this.getData} data={this.props.data && this.props.data.filter(obj => obj.section == 2)} />

                <Spirit getData={this.getData} data={this.props.data && this.props.data.filter(obj => obj.section == 3)}/>

                <Category keys={null} getData={this.getData} data={this.props.data && this.props.data.filter(obj => obj.section == 4)} />

                {/*<GeoLocation />*/}
            </main>
        );
    }
}

const mapStateToProps = (state: any): Props => {
    return {
        data: getHome(state),
        error: getError(state),
    }
};

const mapDispatchToProps = (dispatch): typeof HomeActionCreators => {
    return bindActionCreators(HomeActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
