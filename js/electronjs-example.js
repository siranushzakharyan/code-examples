const { ipcRenderer } = require('electron');
const $bottomSectionElement = document.getElementById('bottom-section');
const $topSectionElement = document.getElementById('top-section');
const $contractTitleElement = document.getElementById('contract-title');
const $clientNameElement = document.getElementById('client-name');
const $stateElement = document.getElementById('state');
const $closeElement = document.getElementById('close');
const $forElement = document.getElementById('show-for');
const $learnMoreElement = document.getElementById('learn-more');
const $connectionNotificationElement = document.getElementById('connection-notification');
const $trackerNotificationElement = document.getElementById('tracker-notification');
let notifierType = null;

$closeElement.addEventListener("click", (event) => {
    ipcRenderer.send("close-notifier", { notifierType })
});

$learnMoreElement.addEventListener("click", () => {
    ipcRenderer.send("close-notifier", { notifierType, showInfoContent: true })
});

ipcRenderer.on("show-notifier", (event, _notifierType, options = {}) => {
    notifierType = _notifierType;
    switch(notifierType) {
        case "stopped":
            $stateElement.innerHTML = notifierType;
            $forElement.classList.add("d-none");
            $bottomSectionElement.classList.add("d-none");
            $topSectionElement.classList.add("no-border");
            break;
        case "started":
            $stateElement.innerHTML = notifierType;
            $clientNameElement.classList.remove("d-none");
            $contractTitleElement.classList.remove("d-none");
            $clientNameElement.innerHTML = options ? options.clientName : "";
            $contractTitleElement.innerHTML = options ? options.title : "";
            break;
        case "no-connection":
            $topSectionElement.classList.add("no-border");
            $trackerNotificationElement.classList.add("d-none");
            $clientNameElement.classList.add("d-none");
            $contractTitleElement.classList.add("d-none");
            $learnMoreElement.classList.remove("d-none");
            $connectionNotificationElement.classList.remove("d-none");
            break;
    }
});
